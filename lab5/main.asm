; Ввод - знаковое 10 с/с
; Вывод - беззнаковое 16 с/с, знаковое в 2 с/с
extrn inputNumber: near
extrn transformTo2Proc: near
extrn transformTo16Proc: near
public numberIn10
public numberIn16
public numberIn2
public negFlag
public number
public system10Base
public system2Base
public endLine
public buffer
public output2Flag

Sseg segment para stack 'stack'
    db 100 dup(0)
Sseg ends

Dseg segment para public 'data'
    inputNumberMessage db "1 - input sign number in 10 n/s$"
    transformTo16 db "2 - transform to no sign 16 n/s$"
    transformTo2 db "3 - transform to sign 2 n/s$"
    showMenu db "4 - show commands$"
    exit db "5 - exit$"
    endLineStr db 0Dh, 0Ah, '$'
    menuInvite db "Input command code(1 - 5):$"

    command db 1 dup(0) ; текущая команда
    commandsArray dw 5 dup(0)

    number dw 1 dup(0)
    system10Base dw 000Ah
    system2Base dw 0002h
    numberIn10 db 6 dup(0) ; число в 10 с/с
    numberIn16 db 4 dup(0) ; число в 16 с/с
    numberIn2 db 17 dup(0) ; число в 2 с/с
    negFlag db 1 dup(0) ; 0 - если число положительно, 1 - если отрицательное
    buffer dw 1 dup(0); вспомогательный буфер
    output2Flag db 1 dup(0); флаг вывода для 2 с/с
Dseg ends

Cseg segment para public 'code'
    assume cs:Cseg, ds: Dseg, ss:Sseg

endLine proc near
    mov ah, 9
    mov dx, offset endLineStr
    int 21h

    ret
endLine endp

showMenuProc proc near
    mov ah, 9
    mov dx, offset inputNumberMessage
    int 21h
    call endLine

    mov dx, offset transformTo16
    int 21h
    call endline

    mov dx, offset transformTo2
    int 21h
    call endLine

    mov dx, offset showMenu
    int 21h
    call endLine

    mov dx, offset exit
    int 21h
    call endLine

    ret
showMenuProc endp

inputCommand proc near
    mov ah, 9
    mov dx, offset menuInvite
    int 21h
    call endLine

    mov ah, 1
    int 21h
    mov command, al
    call endline

    ret
inputCommand endp    

main:
    mov ax, Dseg
    mov ds, ax

    mov ax, Sseg
    mov ss, ax

    mov commandsArray[0], inputNumber
    mov commandsArray[2], transformTo16Proc
    mov commandsArray[4], transformTo2Proc
    mov commandsArray[6], showMenuProc 
    mov commandsArray[8], programQuit

    call showMenuProc

mainLoop:
    call inputCommand

    mov bx, 0
    mov bl, command
    sub bl, 31h
    add bx, bx
    call commandsArray[bx]

    jmp mainLoop

programQuit proc near
    mov ah, 4Ch
    int 21h
programQuit endp
Cseg ends
end main