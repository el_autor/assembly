public transformTo16Proc
extrn numberIn2: byte
extrn numberIn16: byte
extrn system2Base: word
extrn transformTo2Proc: near
extrn endLine: near
extrn buffer: word
extrn output2Flag: byte

Cseg segment para public 'code'

correctCode:
    sub dl, 07h

    jmp correctCodeBack

add2Digit proc near

    mov dx, ax
    mul numberIn2[bx]
    inc bx
    add cx, ax
    mov ax, dx
    mul system2Base

    ret
add2Digit endp

show16Number proc near
    mov bx, 4
    mov ah, 2

show16digit:
    dec bx
 
    mov dl, numberIn16[bx]
    cmp numberIn16[bx], 40h
    jbe correctCode
correctCodeBack::
    int 21h

    cmp bx, 0
    jne show16digit

    call endLine

    ret    
show16Number endp

transformTo16Proc proc near
    mov output2Flag, 1h
    call transformTo2Proc
    mov output2Flag, 0h

    mov bx, 0
    mov buffer, 0

get16Digit:
    mov ax, 1
    mov cx, 0

    call add2Digit
    call add2Digit
    call add2Digit
    call add2Digit

    xchg buffer, bx
    mov numberIn16[bx], cl
    add numberIn16[bx], 37h
    xchg buffer, bx
    inc buffer

    cmp bx, 16
    jne get16Digit

    call show16Number

    ret
transformTo16Proc endp

Cseg ends
end