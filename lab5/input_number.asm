public inputNumber
extrn number: word
extrn numberIn10: byte
extrn negFlag: byte 
extrn system10base: word

Cseg segment para public 'code'
inputSymb proc near
    mov ah, 1
    int 21h

    mov numberIn10[bx], al

    ret
inputSymb endp

inputNumber proc near
    mov number, 0
    mov negFlag, 0
    mov bx, 0
    call inputSymb

    cmp numberIn10[0], '-'
    je setFlag

    sub numberIn10[0], 30h
    inc bx

inputLoop:: ; метка inputLoop локальная, поэтому для jmp снаружи процедуры она видна через 2 двоеточия
    call inputSymb
    sub numberIn10[bx], 30h
    inc bx

    cmp al, 0Dh
    jne inputLoop 

    dec bx
    mov numberIn10[5], bl

    mov ax, 1
    mov cx, ax

makeInt: ; число преобразутся из символьного в числовое представление 
    dec bx
    mov dx, 0
    mov dl, numberIn10[bx] 
    mul dx
    add number, ax
    mov ax, cx
    mul system10base
    mov cx, ax

    cmp bx, 0
    jne makeInt
    
    ;mov ah, 2
    ;mov dl, negFlag
    ;add dl, 30h
    ;int 21h
    ;mov dl, ' '
    ;int 21h
    ;mov dl, numberIn10[5]
    ;add dl, 30h
    ;int 21h

    ret
inputNumber endp

setFlag:
    mov negFlag, 1
    jmp inputLoop

Cseg ends
end