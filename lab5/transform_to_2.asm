public transformTo2Proc
extrn numberIn2: byte
extrn number: word
extrn system2Base: word
extrn negFlag: byte
extrn endLine: near
extrn output2Flag: byte

Cseg segment para public 'code'

nextDigit:
    mov numberIn2[bx], 0
    inc bx

    jmp addCycle

makeAdditionCode proc near
    mov bl, 0

addCycle::
    cmp numberIn2[bx], 1
    je nextDigit

    inc numberIn2[bx]

    ret
makeAdditionCode endp

inverseDigits proc near
inverseLoop:
    dec bx
    not numberIn2[bx]
    sub numberIn2[bx], 0FEh

    cmp bx, 0
    jne inverseLoop

    ret
inverseDigits endp

makeNeg:
    mov bl, numberIn2[16]

    call inverseDigits
    call makeAdditionCode

    jmp makeNegBack

transformTo2Proc proc near
    mov ax, number
    mov bx, 0
    
; перевод из 10 в 2 систему
get2Digit:
    mov dx, 0
    div system2Base

    mov numberIn2[bx], dl
    inc bx

    cmp ax, 0
    jne get2Digit

    cmp bx, 16
    je fullFilled

fillEmptis:
    mov numberIn2[bx], 0
    inc bx
    
    cmp bx, 16
    jne fillEmptis

fullFilled:
    mov numberIn2[16], bl
    mov bx, 0
    add bl, negFlag
    add bl, output2Flag
    cmp bx, 2
    je makeNeg

makeNegBack::

    mov bl, numberIn2[16]
    cmp output2Flag, 1h
    je return

    cmp negFlag, 1
    je showMinus

showIn2::
    dec bx
    
    call showDigit

    cmp bx, 0
    jne showIn2 

    call endLine

return:
    ret
transformTo2Proc endp

showDigit proc near
    mov ah, 2

    mov dl, numberIn2[bx]
    add dl, 30h

    int 21h

    ret
showDigit endp
    
showMinus:
    mov ah, 2

    mov dl, '-'
    int 21h

    jmp showIn2

Cseg ends
end