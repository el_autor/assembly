    .text
    .global transform
transform:
    .intel_syntax noprefix    
    # Передача идет через rdi, rsi, rdx
    movq mm0, rdi
    movq mm1, rsi

    cmp rdx, 1
    je byteAdd

    cmp rdx, 2
    je wordAdd

    cmp rdx, 4
    je dwordAdd

    cmp rdx, 8
    je fullAdd

byteAdd:
    paddb mm0, mm1
    jmp exit   

wordAdd:
    paddw mm0, mm1
    jmp exit   

dwordAdd:
    paddd mm0, mm1
    jmp exit

fullAdd:
    add rdi, rsi
    movq mm0, rdi
    jmp exit

exit:
    movq rax, mm0
    ret

    .data
mult:
    .int 1
res:
    .quad 1
