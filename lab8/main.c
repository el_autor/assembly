#include <stdio.h>

#define OK 0 

extern long transform(long base, long power, int key);

int main()
{
    long num1 = 0, num2 = 0;
    int key = 0;
    
    printf("Введите 2 целых числа и ключ:");
    scanf("%li %li %d", &num1, &num2, &key);

    printf("%li", transform(num1, num2, key));

    return OK;
}