EXTRN input_str: far
PUBLIC buffer

SSeg SEGMENT PARA STACK 'STACK'
    db 100 dup (?)
SSeg ENDS

DSeg SEGMENT PARA PUBLIC 'DATA'
    buffer db 10
DSeg ENDS

CSeg SEGMENT PARA PUBLIC 'CODE'
    assume CS:CSeg, DS:DSeg, SS:SSeg
main:
    mov ax, Dseg
    mov ds, ax

    call input_str

    mov ah, 2
    mov dl, buffer[5]
    int 21h

    mov ah, 4Ch
    int 21h
CSeg ENDS
END main