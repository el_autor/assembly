PUBLIC input_str
EXTRN buffer: far

CSeg SEGMENT PARA PUBLIC 'CODE'
    assume CS:CSeg
input_str proc far
    mov dx, offset buffer
    mov ah, 0Ah
    int 21h

    mov ah, 2
    mov dl, 13
	int 21h
	mov dl, 10
	int 21h

    ret
input_str endp
CSeg ENDS
END