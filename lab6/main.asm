Cseg segment
assume cs:Cseg, ds:Cseg, ss:Cseg
org 100h
Start:
    jmp Init ;Инициализация резидента

butCode proc
    cmp ah, 50h
    je activateFunction

    jmp dword ptr cs:[Int21hOriginal] ;Если функция не 50h, то вызывается оригинальный обработчик

activateFunction:
    mov ah, 00h
    int 16h ;Считываем скан-код клавиши
    mov bl, ah

    mov dl, bl
    mov cl, 4
    shr dl, cl; получаем старший байт скан-кода
    add dl, 37h

    call showSymb

    mov dl, bl
    mov cl, 4
    shl dl, cl
    shr dl, cl; получаем младший байт скан-кода
    add dl, 37h
    
    call showSymb

    iret    
butCode endp

showSymb proc
    cmp dl, 40h
    jg exit

    sub dl, 7h
exit:
    mov ah, 2
    int 21h
    ret
showSymb endp

Int21hOriginal dd 1 dup(0)

Init:
    mov ah, 35h
    mov al, 21h
    int 21h ;Получаем адрес оригинального прерывания 21h, он лежит в es:bx

    ;Сохранение в памяти оригинального прерывания
    mov word ptr Int21hOriginal, bx
    mov word ptr Int21hOriginal + 2, es

    ;Непосредственно перехват
    mov ah, 25h
    mov al, 21h
    mov dx, offset butCode
    int 21h

    ;dx указывает на последний байт, оставшийся в памяти 
    mov dx, offset Init
    int 27h; делает программу резидентом

Cseg ends
end Start