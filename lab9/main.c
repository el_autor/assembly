#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define OK 0

#define PLUS 0
#define MINUS 1
#define MUL 2
#define DIV 3

void transform(char action, int arg1, int* arg2)
{   
    switch (action)
    {
        case PLUS:
            *(arg2) = arg1 + *(arg2);
            break;
        
        case MINUS:
            *(arg2) = arg1 - *(arg2);
            break;
        
        case MUL:
            *(arg2) = arg1 * *(arg2);
            break;
        case DIV:
            *(arg2) = arg1 / *(arg2);

            break;
    }
}

int main()
{
    srand(time(NULL));

    int a, b, c, result;
    char act1, act2;

    a = 3;
    b = 6;
    c = 2;

    act1 = 2;
    act2 = 3; 

    transform(act1, a, &b);
    transform(act2, b, &c);

    printf("Введите число:");
    scanf("%d", &result);

    if (result == c)
    {
        printf("Вы угадали!");
    }
    else
    {       
        printf("Ответ неверный");
    }   

    return OK;    
}