    .text
    .global cpy
cpy:
    .intel_syntax noprefix    
    # Передача идет через rdi, rsi, rdx
    cmp rdi, rsi
    je exit

    xchg rdi, rsi

    mov al, 0
    mov rcx, rdx

    mov r10, rdi
    add r10, rdx
    dec r10
    cmp rsi, rdi
    jl copy
    cmp rsi, r10
    jg copy

    add rsi, rdx
    add rdi, rdx
    dec rsi
    dec rdi

backCopy:
    std
    rep movsb
    mov rax, 1
    jmp exit

copy:
    cld
    rep movsb
    mov rax, 0
    jmp exit

exit:
    ret
