#include <stdio.h>

#define OK 0

extern long long cpy(char* from, char* to, int n);

int main()
{
    char s[] = "string";
    char src[] = "elephant";
    char* ptr1 = src + 2;
    char dest[] = "jiraffa";
    char* ptr2 = dest + 2;
    char test[] = "boombox";
    char* ptr3 = "test";
    size_t length, status;

    asm
    (
            ".intel_syntax noprefix\n"
            "mov rdi, %1\n\t"
            "mov %0, 0\n\t"
            "mov al, 0\n\t"
            "mov rcx, -1\n\t"
            "cld\n"
            "repne scasb\n\t"
            "mov %0, rdi\n\t"
            "sub %0, %1\n\t"
            "dec %0\n\t"
            : "=r" (length)
            : "b" (s)
    );

    printf("%lli\n", s);
    printf("string length - %lli\n\n", length);
    
    printf("%s\n", src);
    printf("%s\n", ptr1);

    status = cpy(ptr1, src, 4);
    
    printf("%s\n", src);
    printf("%s\n\n", ptr1);

    printf("%s\n", dest);
    printf("%s\n", ptr2);

    status = cpy(dest, ptr2, 3);

    printf("%s\n", dest);
    printf("%s\n\n", ptr2);

    printf("%s\n", test);
    printf("%s\n", ptr3);

    status = cpy(ptr3, test, 4);

    printf("%s\n", test);
    printf("%s\n\n", ptr3);

    return OK;
}