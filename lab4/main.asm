Sseg segment para stack 'stack'
    db 100 dup (0)
Sseg ends

Dseg segment para public 'data'
    messageRows db "Input rows number(0 - 9):$"
    messageCols db "Input columns number(0 - 9):$"
    messageInput db "Input matrix:$"
    messageOutput db "Result matrix:$"
    endLine db 0Dh, 0Ah, '$'
    rows db 1 dup (0)
    cols db 1 dup (0)
    matrix db 81 dup (0)
Dseg ends

Cseg segment para public 'code'
    assume CS:Cseg, DS:Dseg, SS:Sseg
main:
    mov ax, Dseg
    mov ds, ax
    mov ah, 9
    mov dx, offset messageRows
    int 21h
    mov dx, offset endLine
    int 21h

    mov ah, 1
    int 21h
    mov rows, al 

    mov ah, 9
    mov dx, offset endLine
    int 21h

    mov ah, 9
    mov dx, offset messageCols
    int 21h
    mov dx, offset endLine     
    int 21h

    mov ah, 1
    int 21h
    mov cols, al

    mov ah, 9
    mov dx, offset endLine
    int 21h

    cmp cols, 30h
    je toEnd

    cmp rows, 30h
    je toEnd

    mov ah, 9
    mov dx, offset messageInput
    int 21h
    mov dx, offset endLine
    int 21h

    sub rows, 30h
    sub cols, 30h
    mov cx, 0
    mov cl, rows
    mov bx, 0

    ;ввод матрицы
inputRow:
    mov es, cx
    mov cx, 0
    mov cl, cols

inputSymb:
    mov ah, 1
    int 21h
    mov matrix[bx], al
    inc bx
    mov ah, 2
    mov dl, ' '
    int 21h
    loop inputSymb

    mov cx, es
    mov ah, 9
    mov dx, offset endLine
    int 21h
    loop inputRow


    cmp rows, 1
    je noCalk


    mov ah, 9
    mov dx, offset messageRows
    int 21h




    ;тут происходит вычитание
    mov bl, rows
    mov al, cols
    mul bl
    sub al, cols
    mov cx, ax
    mov bx, 0    

minus:
    mov dx, 0
    add bl, cols
    mov dl, matrix[bx]
    sub bl, cols

    sub matrix[bx], dl
    lahf
    mov dh, 0FFh
    xor ah, dh 
    mov es, cx
    mov cx, 7

rightHook:
    shr ah, 1
    loop rightHook
    
    mov cx, es
    mov al, ah
    mul matrix[bx]
    mov matrix[bx], al
    add matrix[bx], 30h
    inc bx
    loop minus

    ;вывод матрицы
noCalk:
    mov ah, 9
    mov dx, offset messageOutput
    int 21h
    mov dx, offset endLine
    int 21h

    mov cx, 0
    mov cl, rows
    mov bx, 0

outputRow:
    mov es, cx
    mov cx, 0
    mov cl, cols
outputSymb:
    mov ah, 2
    mov dl, matrix[bx]
    inc bx
    int 21h
    mov ah, 2
    mov dl, ' '
    int 21h
    loop outputSymb

    mov cx, es
    mov ah, 9
    mov dx, offset endLine
    int 21h
    loop outputRow
    
toEnd:
    mov ah, 4Ch
    int 21h
Cseg ends
end main